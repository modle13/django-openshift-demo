import os
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

from . import database
from .models import PageView

# Create your views here.

def index(request):
    """Takes an request object as a parameter and creates an pageview object then responds by rendering the index view."""
    hostname = os.getenv('HOSTNAME', 'unknown')
    print(f"attempting to create a record with hostname {hostname}")
    PageView.objects.create(hostname=hostname)

    print(f"got a request; test var value is {os.getenv('A_TEST_KEY', 'key not found')}")

    return render(request, 'welcome/index.html', {
        'hostname': hostname,
        'database': database.info(),
        'count': PageView.objects.count(),
        'env_vars': {
            "DATABASE_SERVICE_NAME": os.getenv('DATABASE_SERVICE_NAME','not found'),
            "DATABASE_ENGINE": os.getenv('DATABASE_ENGINE','not found'),
            "DATABASE_NAME": os.getenv('DATABASE_NAME','not found'),
            "DATABASE_USER": os.getenv('DATABASE_USER','not found'),
            "DATABASE_PASSWORD": os.getenv('DATABASE_PASSWORD','not found'),
            "POSTGRESQL_SERVICE_HOST": os.getenv('POSTGRESQL_SERVICE_HOST','not found'),
            "POSTGRESQL_SERVICE_PORT": os.getenv('POSTGRESQL_SERVICE_PORT','not found'),
        }
    })

def health(request):
    """Takes an request as a parameter and gives the count of pageview objects as reponse"""
    return HttpResponse(PageView.objects.count())
